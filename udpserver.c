#include <stdio.h>
#include <sys/socket.h>
#include <strings.h>
#include <netinet/in.h>

int main()
{
    int sfd, n;
    socklen_t len;
    // Buffer that will be used to accumulate all incoming UDP packets
    char line[128];
    struct sockaddr_in saddr, caddr;
    // Create a UDP socket
    sfd = socket(AF_INET, SOCK_DGRAM, 0);
    bzero(&saddr, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    saddr.sin_port = htons(2910);
    // Bind socket and address
    bind(sfd, (struct sockaddr *)&saddr, sizeof(saddr));
    printf("Server running\n");
    for(;;) {
        len=sizeof(caddr);
        // Receive data from client
        n=recvfrom(sfd, line, 128, 0, (struct sockaddr *)&caddr, &len);
        // Send data to client
        sendto(sfd, line, n, 0, (struct sockaddr *)&caddr, len);
    }
    return 0;
}