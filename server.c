#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

int main() {
    // Socket file descriptors for the server and the client
    int sfd, cfd;
    int ch = 'k';
    // Socket address for server
    struct sockaddr_in saddr;

    // Create the TCP/IP socket
    sfd = socket(AF_INET, SOCK_STREAM, 0);
    // Use IPv4 protocol family
    saddr.sin_family = AF_INET;
    // Make server listen on any NIC
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    // Make server listen on port 29008
    saddr.sin_port = htons(29008);

    // Bind the sfd descriptor with the address saddr.
    bind(sfd, (struct sockaddr *) &saddr, sizeof(saddr));

    // Mark the socket referred to by sfd as a passive socket.
    listen(sfd, 1);
    while (1) {
        printf("Server waiting\n");

        // Accept extracts the first connection request on the queue of pending connections for the listening socket sfd,
        // creates a new connected socket, and returns a new descriptor referring to that socket — in our program, cfd.
        cfd = accept(sfd, (struct sockaddr *) NULL, NULL);

        // Read the character ch from the descriptor
        if (read(cfd, &ch, 1) < 0) perror("read");

        // Increment the character ch
        ch++;

        // Send the incremented character back to the client
        if (write(cfd, &ch, 1) < 0) perror("write");

        // Close the client socket file descriptor
        close(cfd);
    }
}