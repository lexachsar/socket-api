#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

int main() {
    // Socket file descriptors
    int sfd, cfd;
    // Character that being transmitted
    char ch;
    socklen_t len;
    // IPv6 addresses
    struct sockaddr_in6 saddr, caddr;
    // Create the socket and get the socket file descriptor
    sfd = socket(AF_INET6, SOCK_STREAM, 0);
    saddr.sin6_family = AF_INET6;
    saddr.sin6_addr = in6addr_any;
    saddr.sin6_port = htons(1205);
    // Bind socket and ipv6 address
    bind(sfd, (struct sockaddr *) &saddr, sizeof(saddr));
    // Mark the socket referred to by sfd as a passive socket.
    listen(sfd, 5);
    while (1) {
        printf("Waiting...n");
        len = sizeof(cfd);
        // Extract the first connection request on the queue of pending connections.
        cfd = accept(sfd, (struct sockaddr *) &caddr, &len);
        // Read the character from the client socket
        if (read(cfd, &ch, 1) < 0) perror("read");
        ch++;
        if (write(cfd, &ch, 1) < 0) perror("write");
        close(cfd);
    }
}