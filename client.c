#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(int argc, char *argv[]) {
    // Client socket file descriptor
    int cfd;
    struct sockaddr_in addr;
    char ch = 'r';
    // Create client TCP/IP socket file descriptor
    cfd = socket(AF_INET, SOCK_STREAM, 0);
    addr.sin_family = AF_INET;
    // Client will call server on the localhost
    addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    // Client will use server port 29008
    addr.sin_port = htons(29008);
    // Call connect() system call to connect the socket referred to by sfd to the address specified by addr.
    // The returned descriptor will be used to communicate to the specified address.
    if (connect(cfd, (struct sockaddr *) &addr,
                sizeof(addr)) < 0) {
        perror("connect error");
        return -1;
    }

    // Send character to the server
    if (write(cfd, &ch, 1) < 0) perror("write");
    // Read character from the server
    if (read(cfd, &ch, 1) < 0) perror("read");

    printf("\nReply from Server: %c\n\n", ch);
    // Close client socket
    close(cfd);
    return 0;
}