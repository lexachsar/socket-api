#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

int main(int argc, char **argv) {
    // Client socket file descriptor
    int cfd;
    struct sockaddr_in6 addr;
    char ch;
    if (argc != 3) {
        printf("Usage: %s in6addr charactern", argv[0]);
        return -1;
    }
    // Read server address as cmd argument and convert it to binary form
    if (!inet_pton(AF_INET6, argv[1], &(addr.sin6_addr))) {
        printf("Invalid Addressn");
        return -1;
    }
    // Read character as cmd argument
    ch = argv[2][0];
    // Create client socket
    cfd = socket(AF_INET6, SOCK_STREAM, 0);
    addr.sin6_family = AF_INET6;
    addr.sin6_port = htons(1205);
    // Connect to the server
    if (connect(cfd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        perror("connect error");
        return -1;
    }
    // Write character to the server
    if (write(cfd, &ch, 1) < 0) perror("write");
    // Read character from the server
    if (read(cfd, &ch, 1) < 0) perror("read");
    printf("Server sent: %cn", ch);
    close(cfd);
    return 0;
}