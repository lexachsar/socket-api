#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <arpa/inet.h>

int main() {
    // Socket file descriptors
    int sfd, cfd;
    socklen_t len;
    char ch, buff[INET_ADDRSTRLEN];
    struct sockaddr_in saddr, caddr;
    // Create TCP/IPv4 server socket
    sfd = socket(AF_INET, SOCK_STREAM, 0);
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    saddr.sin_port = htons(29008);
    // Bind socket and socket address
    bind(sfd, (struct sockaddr *) &saddr, sizeof(saddr));
    // Mark the socket referred to by sfd as a passive socket.
    listen(sfd, 5);
    signal(SIGCHLD, SIG_IGN);
    while (1) {
        printf("Server waiting\n");
        len = sizeof(caddr);
        // Accept extracts the first connection request on the queue of pending connections for the listening socket sfd,
        // creates a new connected socket, and returns a new descriptor referring to that socket — in our program, cfd.
        cfd = accept(sfd, (struct sockaddr *) &caddr, &len);
        // Fork the server process to make the server concurrent
        if (fork() == 0) {
            printf("Child Server Created Handling connection with %s\n",
                   inet_ntop(AF_INET, &caddr.sin_addr, buff, sizeof(buff)));
            // Close the server socket
            close(sfd);
            // Read char from the client
            if (read(cfd, &ch, 1) < 0) perror("read");
            while (ch != EOF) {
                if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
                    ch ^= 0x20;
                if (write(cfd, &ch, 1) < 0) perror("write");
                if (read(cfd, &ch, 1) < 0) perror("read");
            }
            // CLose the client socket
            close(cfd);
            return 0;
        }
        close(cfd);
    }

}