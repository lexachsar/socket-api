Code from this repository is based on the following articles:

1. [Creating Your Own Server: The Socket API, Part 1](https://www.opensourceforu.com/2011/08/creating-your-own-server-the-socket-api-part-1/).

2. [Creating Your Own Server: The Socket API, Part 2](https://www.opensourceforu.com/2011/09/creating-your-own-server-the-socket-api-part-2/).

3. [The Socket API, Part 3: Concurrent Servers](https://www.opensourceforu.com/2011/10/socket-api-part-3-concurrent-servers/).

4. [The Socket API, Part 4: Datagrams](https://www.opensourceforu.com/2011/11/socket-api-part-4-datagrams/).

5. [The Socket API, Part 5: SCTP](https://www.opensourceforu.com/2011/12/socket-api-part-5-sctp/).